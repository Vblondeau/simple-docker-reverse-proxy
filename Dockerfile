FROM httpd:2.4


COPY  httpd.conf /usr/local/apache2/conf/httpd.conf

ENV SSL="off"
ENV PORT=80

COPY setup.sh /usr/local/bin/setup.sh
RUN chmod +x /usr/local/bin/setup.sh

CMD ["setup.sh"]
