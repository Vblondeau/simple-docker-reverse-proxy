## Simple docker reverse proxy ##

This projet contain a simple docker proxy.

It redirect localhost adress to another adress.

This container use environment variable to define the proxy target:

environment:

- TARGET_ADDRESS: must be a complete adress including communication protocl
- PORT: the targeted port on localhost for the reverse proxy. defautl is 80
 

How to run the container:


proxy this adress to  localhost:8888 using docker port forwarding:
```
docker run -d  --name=reverse-proxy \
    -p 8888:80  \
    -e TARGET_ADDRESS=https://gitlab.com/Vblondeau/simple-docker-reverse-proxy \
    feanor21/httpd:reverse-proxy 
```

proxy this adress to  localhost:8888 using docker port forwarding and env PORT:
```
docker run -d  --name=reverse-proxy \
    -p 8888:8888 \
    -e PORT=8888  \
    -e TARGET_ADDRESS=https://gitlab.com/Vblondeau/simple-docker-reverse-proxy \
    feanor21/httpd:reverse-proxy 
```
