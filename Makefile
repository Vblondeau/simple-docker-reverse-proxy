IMAGE_NAME=feanor21/httpd
IMAGE_TAG=reverse-proxy
TARGET=https://www.google.fr/



build:
	echo 'building docker image'
	docker build -t  $(IMAGE_NAME):$(IMAGE_TAG) .

build-force:
	echo 'building docker image'
	docker build --no-cache -t  $(IMAGE_NAME):$(IMAGE_TAG) .
	
		
push:
	echo 'pushing docker image'
	docker push $(IMAGE_NAME):$(IMAGE_TAG) 
	
run:
	echo 'running docker image'
	docker run -it  -e TARGET_ADDRESS=$(TARGET) -e $(IMAGE_NAME):$(IMAGE_TAG) 
	
run-dev:
	echo 'running docker image'
	docker run -it -e TARGET_ADDRESS=$(TARGET) $(IMAGE_NAME):$(IMAGE_TAG) bash


run-unit-test:
	echo 'running docker image'
	-docker run --rm -it  -e TARGET_ADDRESS=$(TARGET) -e PORT="toto" $(IMAGE_NAME):$(IMAGE_TAG)
	-docker run --rm -it  -e TARGET_ADDRESS=$(TARGET) -e PORT="100000" $(IMAGE_NAME):$(IMAGE_TAG)
	-docker run --rm -it  -e TARGET_ADDRESS=$(TARGET) -e SSL="on" $(IMAGE_NAME):$(IMAGE_TAG)
