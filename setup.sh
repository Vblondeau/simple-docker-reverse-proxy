#!/bin/bash

if [ -z "$TARGET_ADDRESS" ]
  then 
    echo 'TARGET_ADDRESS not set. no target specified, ending'
    exit 1
  else
    sed -i "s|TARGET|$TARGET_ADDRESS|g" /usr/local/apache2/conf/httpd.conf
fi

if ! [ "$PORT" -eq "$PORT" ]
  then 
  echo "PORT is set to invalid value \"$PORT\", exiting"
  exit 1
fi

if ! (("$PORT" >=1 && "$PORT"<=65535))
  then
  echo "PORT not in range. Exiting"
  exit 1
fi

sed -i "s|PORT|$PORT|g" /usr/local/apache2/conf/httpd.conf
  
  
if [ "$SSL"="off" ]
  then
    sed -i "s|#SSL|SSL|g" /usr/local/apache2/conf/httpd.conf
fi  


/usr/local/bin/httpd-foreground
